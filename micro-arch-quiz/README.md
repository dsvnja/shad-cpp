# Викторина по микроархитектуре

![Intel CPU](https://i.stack.imgur.com/QK4gm.jpg)

## Задание

В это задаче не надо писать код. Нужно его читать и запускать :D

В файлах `bench.cpp` и `bench_*.cpp` лежит набор бенчмарков. Каждый бенчмарк - это одна
функция. Бенчмарк запускается с разными параметрами. Вам нужно
объяснить, почему время работы так сильно (или не сильно) меняется от изменения параметра.

* _Помните, что google-benchmark измеряет только время работы основного_
  _цикла. Время работы инициализации нам не интересно._
* Не забудьте, что компилировать код нужно в Release.

`bench_matrix` может выполняться довольно долго, это нормально.

Чтобы сдать задачу, 
перейдите в ваш репозиторий. Зайдите в merge requests и сделайте merge request из submits/micro-arch-quiz в initial.
Не нажимайте кнопки Merge или Close на созданном Merge Request! В Assignee поставьте danlark.

## Результаты запуска на нашей машине

### bench

```
Run on (4 X 2677.73 MHz CPU s)
2017-03-31 15:19:58
***WARNING*** CPU scaling is enabled, the benchmark real time measurements may be noisy and will incur extra overhead.
Benchmark                      Time           CPU Iterations
------------------------------------------------------------
Lermontov<0>                   3 ns          2 ns  320118723
Lermontov<1>                  19 ns         18 ns   35932729
Tolstoy<1>                     2 ns          2 ns  326062459
Tolstoy<32>                   11 ns         11 ns   52438919
Mayakovsky/255                 2 ns          2 ns  335059044
Mayakovsky/256                 5 ns          5 ns  147439542
Mayakovsky/257                 2 ns          2 ns  339719823
Mayakovsky/511                 3 ns          3 ns  257480133
Mayakovsky/512                 7 ns          7 ns  103705938
Mayakovsky/513                 2 ns          2 ns  307629178
Dostoyevsky<0>                 2 ns          2 ns  329884299
Dostoyevsky<1>                 6 ns          6 ns  118123242
Dostoyevsky<2>                88 ns         88 ns    7867421
Pushkin<0>                    24 ns         24 ns   29948041
Pushkin<1>                    62 ns         62 ns   11133153
Bulgakov<0>/threads:1          7 ns          7 ns   92908411
Bulgakov<0>/threads:2         24 ns         47 ns   15076246
Bulgakov<1>/threads:1          7 ns          7 ns   92812471
Bulgakov<1>/threads:2          4 ns          8 ns   75292746
Gorky<0>                     427 ns        427 ns    1620020
Gorky<1>                     614 ns        614 ns    1122639
```

### bench_float

```
Run on (8 X 800.062 MHz CPU s)
2017-04-14 20:29:32
***WARNING*** CPU scaling is enabled, the benchmark real time measurements may be noisy and will incur extra overhead.
Benchmark           Time           CPU Iterations
-------------------------------------------------
run/0               4 ns          4 ns  154266801
run/1               4 ns          4 ns  192670523
run/2               4 ns          4 ns  192746020
run/3              45 ns         45 ns   15464599
```

### bench_scalar

```
Run on (8 X 3800 MHz CPU s)
CPU Caches:
  L1 Data 32K (x4)
  L1 Instruction 32K (x4)
  L2 Unified 256K (x4)
  L3 Unified 6144K (x1)
***WARNING*** CPU scaling is enabled, the benchmark real time measurements may be noisy and will incur extra overhead.
--------------------------------------------------
Benchmark           Time           CPU Iterations
--------------------------------------------------
First           32261 ns      32253 ns      21401
Second           5709 ns       5708 ns     117030
```

### bench_search

```
Run on (8 X 800.062 MHz CPU s)
2017-04-14 20:32:57
***WARNING*** CPU scaling is enabled, the benchmark real time measurements may be noisy and will incur extra overhead.
Benchmark              Time           CPU Iterations
----------------------------------------------------
simple_search        377 ns        377 ns    1871495
block_search         237 ns        237 ns    2943895
```

### bench_matrix
```
Run on (8 X 900.07 MHz CPU s)
2017-04-14 20:33:45
***WARNING*** CPU scaling is enabled, the benchmark real time measurements may be noisy and will incur extra overhead.
Benchmark                Time           CPU Iterations
------------------------------------------------------
run<0>/2k            46055 ms      46055 ms          1
run<0>/2.00098k      13750 ms      13750 ms          1
run<1>/2k             2647 ms       2647 ms          1
run<1>/2.00098k       2686 ms       2686 ms          1
```

## Полезные ссылки

 - [CppCon 2016: Timur Doumler “Want fast C++? Know your hardware!"](https://www.youtube.com/watch?v=BP6NxVxDQIs)
 - [x86 Internals for Fun & Profit • Matt Godbolt](https://www.youtube.com/watch?v=hgcNM-6wr34)

