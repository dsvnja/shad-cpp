#include <gtest/gtest.h>
#include <boost/asio.hpp>
#include <vector>
#include "commons.h"

TEST(Client, IsSending) {
    ServerWrapper server(localhost, 32);
    server.Start();
    ClientWrapper client(localhost);
    const int iterations = 10;
    std::vector<int> values;
    for (int i = 1; i <= iterations; ++i) {
        values.push_back(i);
        client.Sum(values);
    }

    server.Shutdown();
    ASSERT_EQ(iterations, server.ProcessedQueryCount());
}

TEST(Client, SumCorrectness) {
    ServerWrapper server(localhost, 32);
    server.Start();
    ClientWrapper client(localhost);
    const int iterations = 100;
    std::vector<int> values;
    for (int i = 1; i <= iterations; ++i) {
        values.push_back(i);
        int sum = client.Sum(values);
        ASSERT_EQ(i * (i + 1) / 2, sum);
    }
}
