cmake_minimum_required(VERSION 2.8)
project(socks4)

set(SRCS socks4.cpp)
set(MAIN_SRCS server_main.cpp)
set(TEST_SRCS test.cpp)

if (TEST_SOLUTION)
  include_directories(../private/socks4-proxy)
  set(SRCS ../private/socks4-proxy/socks4.cpp)
  set(MAIN_SRCS ../private/socks4-proxy/server_main.cpp)
endif()

if (ENABLE_PRIVATE_TESTS)
endif()

include(../common.cmake)

add_library(socks4 ${SRCS})
target_link_libraries(socks4
  boost_system
  glog)

add_gtest(test_socks ${TEST_SRCS})
target_link_libraries(test_socks socks4)

add_executable(socks4_server ${MAIN_SRCS})
target_link_libraries(socks4_server socks4)
